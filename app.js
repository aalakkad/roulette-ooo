#!/usr/bin/env node

const dotenv = require("dotenv");
const fetch = require("node-fetch");
const fs = require("fs");

dotenv.config();

// ensure Slack token exists
if (!process.env.SLACK_TOKEN) {
  console.error("SLACK_TOKEN not found");
  process.exit(1);
}

// get roulette.json
async function getRouletteData() {
  try {
    const response = await fetch("https://about.gitlab.com/roulette.json");
    const roulette = await response.json();
    return roulette;
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
}

// get slack's users
async function getSlackUsers() {
  try {
    const response = await fetch(
      `https://slack.com/api/users.list?token=${process.env.SLACK_TOKEN}&pretty=1`
    );
    const json = await response.json();
    let members = json.members;
    // normalize members data
    members = members
      .map(member => ({
        name: member.name,
        email: member.profile.email,
        status_text: member.profile.status_text,
        status_emoji: member.profile.status_emoji
      }))
      .filter(member => !member.deleted);

    return members;
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
}

getRouletteData().then(rouletteData => {
  getSlackUsers().then(members => {
    const OOOs = members
      .filter(member => member.status_text.includes("OOO"))
      .filter(member => member.status_text.includes("Public Holiday"))
      .filter(member => member.status_text.includes("Vacation"))
      .filter(member => member.status_text.includes("Out Sick"))
      .filter(member => member.status_text.includes("Bereavement"))
      .map(member => member.name);
    const OOCs = members
      .filter(member => member.status_emoji === ":red_circle:")
      .map(member => member.name);

    // Loop over the roulette items and process
    const roulette = rouletteData.map(item => {
      if (OOOs.includes(item.username) || OOCs.includes(item.username)) {
        item.available = false;
      }

      return item;
    });

    // Save the file
    fs.writeFile("public/roulette.json", JSON.stringify(roulette), err => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
    });
  });
});
