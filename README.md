>💡 Dangerbot respects OOO.

# Brief

A project that checks Slack’s API and exclude people that are OOO from the reviewer roulette data.

# Concerns

- How to match Slack’s username with GitLab’s username?
    - Assuming that Slack’s `name` is the same as GitLab’s `username`.
- How Danger bot will read the modified roulette data?
    - Either it saves new file every x minutes that replaces the current file
    - Or it publishes a new trimmed file to a different location, i.e. GitLab’s Pages.

# Resources


- [https://api.slack.com/methods/users.list](https://api.slack.com/methods/users.list)
- [https://api.slack.com/docs/rate-limits](https://api.slack.com/docs/rate-limits)
- [Current roulette](https://about.gitlab.com/roulette.json)
- [Inspiration project](https://gitlab.com/leipert-projects/is-gitlab-pretty-yet/blob/master/analysis.sh)
- [https://docs.gitlab.com/ee/user/project/pages/](https://docs.gitlab.com/ee/user/project/pages/)
- [https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names)
- [https://docs.gitlab.com/ee/user/project/pages/getting_started/new_or_existing_website.html](https://docs.gitlab.com/ee/user/project/pages/getting_started/new_or_existing_website.html)
- [https://about.gitlab.com/blog/2016/11/03/publish-code-coverage-report-with-gitlab-pages/](https://about.gitlab.com/blog/2016/11/03/publish-code-coverage-report-with-gitlab-pages/)